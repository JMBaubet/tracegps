#include "styleapplication.h"

StyleApplication::StyleApplication() {
    enum Material {Red, Pink, Purple, DeepPurple, Indigo, Blue, LightBlue, Cyan, Teal, Green, LightGreen, Lime, Yellow, Amber, Orange, DeepOrange, Brown, Grey, BlueGrey};

    //qDebug() << "StyleApplication : Constructeur";


    m_accent = Red;
    m_theme = 0;

    QSettings setting("jmbaubet.com","traceGPS");
    setting.beginGroup("StyleApplication");
    m_accent = setting.value("accent").toInt();
    m_theme = setting.value("theme").toInt();
    setting.endGroup();
}

StyleApplication::~StyleApplication() {

}

void StyleApplication::setAccent(int couleur) {
    m_accent = couleur;
    //qDebug() << "StyleApplication::setAccent : " << couleur;
    // On sauvegarde la nouvelle valeur dans le settings
    QSettings setting("jmbaubet.com","traceGPS");
    setting.beginGroup("StyleApplication");
    setting.setValue("accent", m_accent);
    setting.endGroup();
}

int StyleApplication::accent() {
    //qDebug() << "StyleApplication::accent() retourne : " << m_accent;
    return m_accent;
}

void StyleApplication::setTheme(int value) {
    m_theme = value;
    //qDebug() << "StyleApplication::setTheme : " << value;
    // On sauvegarde la nouvelle cvaleur dans le settings
    QSettings setting("jmbaubet.com","traceGPS");
    setting.beginGroup("StyleApplication");
    setting.setValue("theme", m_theme);
    setting.endGroup();

}

int StyleApplication::theme() {
    return m_theme;
}


void StyleApplication::LoadSettings(){
    QSettings settings("","");

}



