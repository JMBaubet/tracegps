import QtQuick 2.0
import QtQuick.Controls 2.4
import QtLocation 5.9
import QtPositioning 5.8

import com.traceGPS.style 1.0
import com.traceGPS.param 1.0


Map {

    id: map
    anchors.fill: parent
    plugin: mapboxglPlugin
    // Les paramètres suivants doivent être enregistrés dans les settings
    center: QtPositioning.coordinate(settingParam.latitude, settingParam.longitude)
    zoomLevel: settingParam.echelle
    minimumZoomLevel: 5
    maximumZoomLevel: 18


    Plugin {
        id: mapboxglPlugin
        name: "mapboxgl"
        PluginParameter {
            id: pluginParameter
            name: "mapboxgl.access_token"
            value: settingParam.mapboxPwd
        }

        PluginParameter {
            name: "mapboxgl.mapping.additional_style_urls"
            value: settingParam.mapModeLight +"," + settingParam.mapModeDark + ",mapbox://styles/mapbox/navigation-preview-night-v2"
        }
    }
    activeMapType: {
        if (popupStyle.switch1ModeSombre.checked) {
            return supportedMapTypes[1]
        } else {
            return supportedMapTypes[0]
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: map
        hoverEnabled: true
        onClicked: {
            console.info("coordonnes : ", map.toCoordinate(Qt.point(mouseX, mouseY)))
        }
        onDoubleClicked: {
            console.info("double click")
        }


        property var coordinate: map.toCoordinate(Qt.point(mouseX, mouseY))
    }



    onZoomLevelChanged: {
//        fileParameters.zoomLevel = zoomLevel;
        settingParam.echelle = zoomLevel
        popupCarte.sliderZoom.value = zoomLevel
        popupCarte.valeurZoom.text = zoomLevel.toFixed(1)
        //console.info("Zoom : ", zoomLevel)
    }

    onCenterChanged: {
        //console.info("centre : ", center )
        settingParam.latitude = center.latitude
        settingParam.longitude = center.longitude
        popupCarte.latitudeCentre.text = settingParam.latitudeGeo
        popupCarte.longitudeCentre.text =settingParam.longitudeGeo
    }


}
