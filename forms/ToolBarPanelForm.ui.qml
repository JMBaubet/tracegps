import QtQuick 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 2.3

Item {
    id: toolBar
    anchors.top: parent.top
    anchors.topMargin: 16
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 32
    anchors.left: parent.left
    anchors.leftMargin: 16
    opacity: 0.8
    width: 48
    z: 5

    property alias pane: pane

    Pane {
        id: pane
        anchors.fill: parent
        Material.elevation: 6
    }
}

