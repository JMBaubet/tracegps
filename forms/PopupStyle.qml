import QtQuick.Window 2.11
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
//import QtQuick.Dialogs 1.2
//import QtQml 2.2
//import Qt.labs.settings 1.0


import com.traceGPS.style 1.0


Window {
    id: popupW
    modality: Qt.WindowModal
    width: 400
    height: 524
    minimumWidth: 400
    maximumWidth: 400
    minimumHeight: 524
    maximumHeight: 821
    opacity: 0.85

    // Définition d'une variable locale
    property int accent: styleApplication.accent
    Material.accent: accent;
    Material.theme: switch1ModeSombre.checked ? Material.Dark : Material.Light

    //exemple d'utlisation de signal entre fichier qml
    //https://stackoverflow.com/questions/40407583/how-to-send-a-signal-from-one-qml-to-another
    signal signalAccentChanged()
    signal signalThemeChanged()

    function changeAccent() {
        //console.info("PopupStyle.qml::accentChange")
        Material.accent = styleApplication.accent;
        popupW.signalAccentChanged()
    }


    property alias switch1ModeSombre: switch1ModeSombre


    Pane {
        id:pane
        anchors.fill: parent

        ColumnLayout {
            id: columnLayout
            spacing: 5
            anchors.fill: parent

            GroupBox {
                id: groupBox
                Layout.fillHeight: true
                font.family: "Tahoma"
                font.pointSize: 14
                Layout.fillWidth: true
                title: qsTr("Paramétrage")


                GridLayout {
                    id: gridLayout
                    anchors.fill: parent
                    columns: 2



                    Label {
                        id: label
                        text: qsTr("Mode Sombre : ")
                        verticalAlignment: Text.AlignVCenter
                        font.family: "Tahoma"
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignRight
                    }

                    Switch {
                        id: switch1ModeSombre
                        x: 0
                        text: qsTr("")
                        padding: 0
                        checked: styleApplication.theme
                        onCheckedChanged: {
                        //        console.info("switchModeSombreChanged")

                                // On mets à jour le theme dans la classe.
                                styleApplication.theme = switch1ModeSombre.checked
                                popupW.signalThemeChanged()

                                couleur1.color = switch1ModeSombre.checked ? "#EF9A9A" : "#F44336"
                                couleur2.color = switch1ModeSombre.checked ? "#F48FB1" : "#E91E63"
                                couleur3.color = switch1ModeSombre.checked ? "#CE93D8" : "#9C27B0"
                                couleur4.color = switch1ModeSombre.checked ? "#B39DDB" : "#673AB7"
                                couleur5.color = switch1ModeSombre.checked ? "#9FA8DA" : "#3F51B5"
                                couleur6.color = switch1ModeSombre.checked ? "#90CAF9" : "#2196F3"
                                couleur7.color = switch1ModeSombre.checked ? "#81D4FA" : "#03A9F4"
                                couleur8.color = switch1ModeSombre.checked ? "#80DEEA" : "#00BCD4"
                                couleur9.color = switch1ModeSombre.checked ? "#80CBC4" : "#009688"
                                couleur10.color = switch1ModeSombre.checked ? "#A5D6A7" : "#4CAF50"
                                couleur11.color = switch1ModeSombre.checked ? "#C5E1A5" : "#8BC34A"
                                couleur12.color = switch1ModeSombre.checked ? "#E6EE9C" : "#CDDC39"
                                couleur13.color = switch1ModeSombre.checked ? "#FFF59D" : "#FFEB3B"
                                couleur14.color = switch1ModeSombre.checked ? "#FFE082" : "#FFC107"
                                couleur15.color = switch1ModeSombre.checked ? "#FFCC80" : "#FF9800"
                                couleur16.color = switch1ModeSombre.checked ? "#FFAB91" : "#FF5722"
                                couleur17.color = switch1ModeSombre.checked ? "#BCAAA4" : "#795548"
                                couleur18.color = switch1ModeSombre.checked ? "#EEEEEE" : "#9E9E9E"
                                couleur19.color = switch1ModeSombre.checked ? "#B0BEC5" : "#607D8B"
                            }
                    }

                    Label {
                        id: label1
                        y: 0
                        text: qsTr("Palette : ")
                        Layout.alignment: Qt.AlignRight | Qt.AlignTop
                    }


                    ScrollView {
                        id: scrollView
                        x: 0
                        y: 0
                        width: 200
                        height: 200
                        Layout.preferredHeight: 0
                        Layout.preferredWidth: 0
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        clip: true

                        GridLayout {
                            id: gridLayout1
                            columnSpacing: 14
                            rowSpacing: -14
                            Layout.fillWidth: true
                            columns: 2

                            Rectangle {
                                id: couleur1
                                width: 30
                                height : 30
                                color: "#F44336"
                            }

                            RadioButton {
                                id: radioButton1
                                text: "Red"
                                checked: accent === Material.Red ? true : false
                                onCheckedChanged:  {
                                    styleApplication.accent = Material.Red
                                    changeAccent()
                                }

                            }

                            Rectangle {
                                id: couleur2
                                width: 30
                                height : 30
                                color: "#E91E63"
                            }
                            RadioButton {
                                id: radioButton2
                                text: qsTr("Pink")
                                checked: accent === Material.Pink ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Pink
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur3
                                width: 30
                                height: 30
                                color: "#9C27B0"
                            }
                            RadioButton {
                                id: radioButton3
                                text: "Purple"
                                checked: accent === Material.Purple ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Purple
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur4
                                width: 30
                                height: 30
                                color: "#673AB7"
                            }
                            RadioButton {
                                id: radioButton4
                                text: "DeepPurple"
                                checked: accent === Material.DeepPurple ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.DeepPurple
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur5
                                width: 30
                                height: 30
                                color: "#3F51B5"
                            }
                            RadioButton {
                                id: radioButton5
                                text: "Indigo"
                                checked: accent === Material.Indigo ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Indigo
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur6
                                width: 30
                                height: 30
                                color: "#2196F3"
                            }
                            RadioButton {
                                id: radioButton6
                                text: "Blue"
                                checked: accent === Material.Blue ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Blue
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur7
                                width: 30
                                height: 30
                                color: "#03A9F4"
                            }
                            RadioButton {
                                id: radioButton7
                                text: "LightBlue"
                                checked: accent === Material.LightBlue ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.LightBlue
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur8
                                width: 30
                                height: 30
                                color: "#00BCD4"
                            }
                            RadioButton {
                                id: radioButton8
                                text: "Cyan"
                                checked: accent === Material.Cyan ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Cyan
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur9
                                width: 30
                                height: 30
                                color: "#009688"
                            }
                            RadioButton {
                                id: radioButton9
                                text: "Teal"
                                checked: accent === Material.Teal ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Teal
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur10
                                width: 30
                                height: 30
                                color: "#4CAF50"
                            }
                            RadioButton {
                                id: radioButton10
                                text: "Green"
                                checked: accent === Material.Green ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Green
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur11
                                width: 30
                                height: 30
                                color: "#8BC34A"
                            }
                            RadioButton {
                                id: radioButton11
                                text: "LightGreen"
                                checked: accent === Material.LightGreen ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.LightGreen
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur12
                                width: 30
                                height: 30
                                color: "#CDDC39"
                            }
                            RadioButton {
                                id: radioButton12
                                text: "Lime"
                                checked: accent === Material.Lime ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Lime
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur13
                                width: 30
                                height: 30
                                color: "#FFEB3B"
                            }
                            RadioButton {
                                id: radioButton13
                                text: "Yellow"
                                checked: accent === Material.Yellow ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Yellow
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur14
                                width: 30
                                height: 30
                                color: "#FFC107"
                            }
                            RadioButton {
                                id: radioButton14
                                text: "Amber"
                                checked: accent === Material.Amber ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Amber
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur15
                                width: 30
                                height: 30
                                color: "#FF9800"
                            }
                            RadioButton {
                                id: radioButton15
                                text: "Orange"
                                checked: accent === Material.Orange ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Orange
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur16
                                width: 30
                                height: 30
                                color: "#FF5722"
                            }
                            RadioButton {
                                id: radioButton16
                                text: "DeepOrange"
                                checked: accent === Material.DeepOrange ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.DeepOrange
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur17
                                width: 30
                                height: 30
                                color: "#795548"
                            }
                            RadioButton {
                                id: radioButton17
                                text: "Brown"
                                checked: accent === Material.Brown ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Brown
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur18
                                width: 30
                                height: 30
                                color: "#9E9E9E"
                            }
                            RadioButton {
                                id: radioButton18
                                text: "Grey"
                                checked: accent === Material.Grey ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.Grey
                                    changeAccent()
                                }
                            }

                            Rectangle {
                                id: couleur19
                                width: 30
                                height: 30
                                color: "#607D8B"
                            }
                            RadioButton {
                                id: radioButton19
                                text: "BlueGrey"
                                checked: accent === Material.BlueGrey ? true : false
                                onCheckedChanged: {
                                    styleApplication.accent = Material.BlueGrey
                                    changeAccent()
                                }
                            }
                        } //GridLayout
                    } //ScrollView

                } //GridLayout
            } //GroupBox


            RowLayout {
                opacity: 1
                id: rowLayout
                y: 0
                Layout.fillWidth: true

                Label {
                    id: label2
                    text: qsTr("Rendu :")
                }

                Switch{
                    checked: true
                }

                Slider {
                    value: 0.9
                    Layout.fillWidth: true
                }

                Button {
                    id: boutonClose
                    text: qsTr("OK")
                    Layout.columnSpan: 15
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    onClicked: popupW.hide()
                }

            } //RowLayout
        } //ColumnLayout
    } //Pane
} // Window


