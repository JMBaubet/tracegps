import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3


import com.traceGPS.style 1.0
import com.traceGPS.param 1.0

Window {
    id: popupCarte
    width: 400
    height: 524
    property alias mapBoxKey: mapBoxKey
    property alias sliderZoom: sliderZoom
    property alias valeurZoom: valeurZoom
    property alias longitudeCentre: longitudeCentre
    property alias latitudeCentre: latitudeCentre
    title: "Paramétrage carte"
    opacity: 0.85
    modality: Qt.WindowModal
    minimumHeight: 524
    minimumWidth: 400
    maximumHeight: 254
    maximumWidth: 400

    // Voir comment on récupère les valeurs du Settings
    Material.theme: styleApplication.theme
    Material.accent: styleApplication.accent

    signal echelleChanged()

    function sauvegarde() {
        popupCarte.hide()
        settingParam.saveCoordonnee()
    }

    function setEchelle() {
        settingParam.echelle = sliderZoom.value
        popupCarte.echelleChanged()
    }

    function changeAccent(){
        //console.info("PopupCarte::changeAccent() : " + styleApplication.accent)
        Material.accent = styleApplication.accent
    }

    function changeTheme(){
        //console.info("PopupCarte::changeTheme() : " + styleApplication.theme)
        Material.theme = styleApplication.theme
    }

    Pane {
        id: pane
        width: 400
        font.family: "Tahoma"
        font.pixelSize: 14
        anchors.fill: parent
        ColumnLayout {
            height: 482
            spacing: 8
            anchors.fill: parent

            GroupBox {
                id: groupBox
                //                width: 200
                //                height: 200
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.fillWidth: true
                title: qsTr("Paramétres MapBox")


                GridLayout {
                    anchors.fill: parent
                    columnSpacing: 0
                    rowSpacing: 0
                    columns: 2

                    Label {
                        id: label
                        text: qsTr("MapBox Key : ")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        font.family: "Tahoma"
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignRight

                    }

                    TextField {
                        id: mapBoxKey
                        text: settingParam.mapboxPwd
                        echoMode: TextInput.PasswordEchoOnEdit
                        Layout.fillWidth: true

                        onTextChanged: settingParam.mapboxPwd = text
                    }

                    Label {
                        text: qsTr("Carte mode clair : ")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignRight
                    }

                    TextField {
                        id: mapModeLight
                        text: settingParam.mapModeLight
                        Layout.fillWidth: true
                        onTextChanged: settingParam.mapModeLight = text
                    }

                    Label {
                        text: qsTr("Carte mode sombre : ")
                        verticalAlignment: Text.AlignVCenter
                        font.family: "Tahoma"
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignRight
                    }

                    TextField {
                        id: mapModeDark
                        text: settingParam.mapModeDark
                        Layout.fillWidth: true
                        onTextChanged: settingParam.mapModeDark = text
                    }


                } //GridLayout
            }



            GroupBox {
                id: groupBox1
                Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true
                title: qsTr("Coordonnées du centre")

                GridLayout {
                    id: gridLayout
                    anchors.fill: parent
                    //                    width: 100
                    //                    height: 100
                    columns: 3


                    Label {
                        id: labelLatitude
                        text: qsTr("Latitude :")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    Label {
                        id: latitudeCentre
                        text: settingParam.latitudeGeo
                        horizontalAlignment: Text.AlignRight
                    }


                    Item {
                        id: item1
                        Layout.fillWidth: true
                    }

                    Label {
                        id: labelLongitude
                        text: qsTr("Longitude :")
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    Label {
                        id: longitudeCentre
                        text: settingParam.longitudeGeo
                        horizontalAlignment: Text.AlignRight
                    }



                    Item {
                        id: item2
                        Layout.fillWidth: true
                    }





                }
            }

            GroupBox {
                id: groupBox2
                Layout.fillWidth: true
                height: 200
                title: qsTr("Facteur de zoom :")

                GridLayout {
                    id: gridLayout1
                    columns: 2
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    columnSpacing: 8
                    Layout.fillWidth: true

                    TextMetrics {
                        id: size3Chars
                        text: "18.0"
                    }

                    Label {
                        id: valeurZoom
                        text: settingParam.echelle.toFixed(1)
                        Layout.maximumWidth: size3Chars.width.toFixed(0)
                        Layout.minimumWidth: size3Chars.width.toFixed(0)
                        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    }

                    Slider {
                        id: sliderZoom
                        stepSize: 0.1
                        Layout.fillWidth: true
                        to: 18
                        from: 5
                        value: settingParam.echelle
                        onMoved: {
                            valeurZoom.text = sliderZoom.value.toFixed(1)
                            setEchelle()
                        }
                    }
                }
            }

            RowLayout {
                Layout.alignment: Qt.AlignRight | Qt.AlignBottom

                Button {
                    id: button
                    text: qsTr("Abandonner")
                    onClicked: popupCarte.hide()
                }

                Button {
                    id: boutonClose
                    text: qsTr("Sauvegarder")
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    //Layout.columnSpan: 15
                    //Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    onClicked: sauvegarde()                }

            }
        }
    }
}
