#include "settingparam.h"

SettingParam::SettingParam() {
    //qDebug() << "SettingParam : Constructeur";
    QSettings setting("jmbaubet.com","traceGPS");
    setting.beginGroup("SettingParam");
    m_echelle = setting.value("echelle").toFloat();
    m_latitude = setting.value("latitude").toFloat();
    m_longitude = setting.value("longitude").toFloat();
    m_mapboxPwd = setting.value("mapboxPwd").toString();
    m_mapModeLight = setting.value("mapModeLight").toString();
    m_mapModeDark = setting.value("mapModeDark").toString();
    setting.endGroup();
    m_coordonneeCentre.setLatitude(double(m_latitude));
    m_coordonneeCentre.setLongitude(double(m_longitude));
    //qDebug() << "SettingParam::SettingParam()::m_latitude : " << m_latitude;
    //qDebug() << "SettingParam::SettingParam()::m_longitude : " << m_longitude;
    qDebug() << "SettingParam::SettingParam()::m_mapboxPwd : " << m_mapboxPwd;
}



SettingParam::~SettingParam() {

}

// Parametres


float SettingParam::echelle() {
    return m_echelle;
}

void SettingParam::setEchelle(float echelle) {
    //qDebug() << "SettingParam::setEchelle : " << echelle;
    m_echelle = echelle;
}

float SettingParam::latitude() {
    return m_latitude;
}

void SettingParam::setLatitude(float latitude) {
    //qDebug() << "SettingParam::setLatitude : " << latitude;
    m_latitude = latitude;
    m_coordonneeCentre.setLatitude(double(latitude));
}

QString SettingParam::longitudeGeo() {
    //qDebug() << "SettingParam::longitudeGeo : " << m_coordonneeCentre.toString(QGeoCoordinate::DegreesMinutesSecondsWithHemisphere);
    QStringList list =  m_coordonneeCentre.toString(QGeoCoordinate::DegreesMinutesSecondsWithHemisphere).split(QRegExp(", "));
    return list[1];
}


void SettingParam::setLongitudeGeo(QString longitudeGeo) {
    m_longitudeGeo = longitudeGeo;
}

float SettingParam::longitude() {
    return m_longitude;
}


void SettingParam::setLongitude(float longitude) {
    //qDebug() << "SettingParam::setLongitue : " << m_coordonneeCentre.toString(QGeoCoordinate::DegreesMinutesSecondsWithHemisphere);
    m_longitude = longitude;
    m_coordonneeCentre.setLongitude(double(longitude));
}

QString SettingParam::latitudeGeo() {
    //qDebug() << "SettingParam::latitudeGeo : " << m_coordonneeCentre.toString(QGeoCoordinate::DegreesMinutesSecondsWithHemisphere);
    QStringList list =  m_coordonneeCentre.toString(QGeoCoordinate::DegreesMinutesSecondsWithHemisphere).split(QRegExp(", "));
    return list[0];
}

void SettingParam::setLatitudeGeo(QString latitudeGeo) {
    m_latitudeGeo = latitudeGeo;
}


void SettingParam::saveCoordonnee() {
    //qDebug() << "SettingParam::saveCoordonnee"  << m_latitude << " " << m_longitude;
    QSettings setting("jmbaubet.com","traceGPS");
    setting.beginGroup("SettingParam");
    setting.setValue("echelle", m_echelle);
    setting.setValue("longitude", m_longitude);
    setting.setValue("latitude", m_latitude);
    setting.setValue("mapboxPwd", m_mapboxPwd);
    setting.setValue("mapModeLight", m_mapModeLight);
    setting.setValue("mapModeDark", m_mapModeDark);
    setting.endGroup();
}

QString SettingParam::mapboxPwd() {
    return m_mapboxPwd;
}

void SettingParam::setMapboxPwd(QString mapboxPwd) {
    m_mapboxPwd = mapboxPwd;
}

QString SettingParam::mapModeDark() {
    return m_mapModeDark;
}

void SettingParam::setMapModeDark(QString mode) {
    m_mapModeDark = mode;
}

QString SettingParam::mapModeLight() {
    return m_mapModeLight;
}

void SettingParam::setMapModeLight(QString mode) {
    m_mapModeLight = mode;
}
