#ifndef STYLEAPPLICATION_H
#define STYLEAPPLICATION_H

#include <QQuickItem>
#include <QWidget>
#include <QtQuick>
#include <QtQml>
#include <QSettings>

class StyleApplication : public QObject
{


    Q_OBJECT
    Q_PROPERTY(int accent READ accent WRITE setAccent NOTIFY accentChanged)
    Q_PROPERTY(int theme READ theme WRITE setTheme NOTIFY themeChanged)

public:
    StyleApplication();
    virtual ~StyleApplication();

signals:
    void accentChanged();
    void themeChanged();


private:

    void LoadSettings();

    int m_accent;
    int accent();
    void setAccent(int couleur);

    int m_theme;
    int theme();
    void setTheme(int value);

};

#endif // STYLEAPPLICATION_H
