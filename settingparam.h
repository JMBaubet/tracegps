#ifndef SETTINGPARAM_H
#define SETTINGPARAM_H

#include <QObject>
#include <QtQml>
#include <QGeoCoordinate>
#include <QString>


class SettingParam : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float echelle READ echelle WRITE setEchelle NOTIFY echelleChanged)
    Q_PROPERTY(float latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)
    Q_PROPERTY(float longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(QString latitudeGeo READ latitudeGeo WRITE setLatitudeGeo NOTIFY latitudeGeoChanged)
    Q_PROPERTY(QString longitudeGeo READ longitudeGeo WRITE setLongitudeGeo NOTIFY longitudeGeoChanged)
    Q_PROPERTY(QString mapboxPwd READ mapboxPwd WRITE setMapboxPwd NOTIFY mapboxPwdChanged)
    Q_PROPERTY(QString mapModeDark READ mapModeDark WRITE setMapModeDark NOTIFY mapModeDarkChanged)
    Q_PROPERTY(QString mapModeLight READ mapModeLight WRITE setMapModeLight NOTIFY mapModeLightChanged)

public:
    explicit SettingParam();
    virtual ~SettingParam();

    Q_INVOKABLE void saveCoordonnee();


signals:
    void echelleChanged();
    void latitudeChanged();
    void longitudeChanged();
    void latitudeGeoChanged();
    void longitudeGeoChanged();
    void mapboxPwdChanged();
    void mapModeDarkChanged();
    void mapModeLightChanged();

public slots:


private:

    float m_echelle;
    float echelle();
    void setEchelle(float echelle);

    QGeoCoordinate m_coordonneeCentre;

    float m_latitude;
    float latitude();
    void setLatitude(float latitude);
    QString m_latitudeGeo;
    QString latitudeGeo();
    void setLatitudeGeo(QString latitude);

    float m_longitude;
    float longitude();
    void setLongitude(float longitude);
    QString m_longitudeGeo;
    QString longitudeGeo();
    void setLongitudeGeo(QString longitude);

    QString m_mapboxPwd;
    QString mapboxPwd();
    void setMapboxPwd(QString mapboxPwq);

    QString m_mapModeDark;
    QString mapModeDark();
    void setMapModeDark(QString mode);

    QString m_mapModeLight;
    QString mapModeLight();
    void setMapModeLight(QString mode);
};



#endif // SETTINGPARAM_H
