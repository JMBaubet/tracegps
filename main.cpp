#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "styleapplication.h"
#include "settingparam.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    // pour le QSettings
    QCoreApplication::setOrganizationName("jmbaubet");
    QCoreApplication::setOrganizationDomain("jmbaubet.com");
    QCoreApplication::setApplicationName("traceGPS");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<StyleApplication>("com.traceGPS.style",1,0,"StyleApplication");
    qmlRegisterType<SettingParam>("com.traceGPS.param",1,0,"SettingParam");

    engine.load(QUrl(QStringLiteral("qrc:/mapviewer.qml")));


    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
