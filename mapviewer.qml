import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import Qt.labs.settings 1.0
import "maps"
import "forms"

import com.traceGPS.style 1.0
import com.traceGPS.param 1.0

ApplicationWindow {
    id: appWindow
    visible: true
    minimumHeight: 555
    minimumWidth: 512
    width: 1920
    height: 1080
    title: qsTr("Trace GPS")

    // On sauvegarde les paramètre de la fenêtre...
    Settings {
        property alias x: appWindow.x
        property alias y: appWindow.y
        property alias height: appWindow.height
        property alias width: appWindow.width
    }

    function changeTheme(){
        popupCarte.changeTheme()
    }

    function changeAccent(){
        Material.accent = styleApplication.accent
        popupCarte.changeAccent()
    }

    function changeSaveSettingMAp() {
        //console.info("mapviewer.qml::changeSaveSettingMap", popupCarte.switch1.checked)
        settingParam.saveSettingMap = popupCarte.switch1.checked
    }

    // On instancie une class StyleApplication pour personnaliser le stye de l'application
    // Information sur https://doc.qt.io/qt-5.11/qtquickcontrols2-material.html
    StyleApplication {
        id: styleApplication        
    }

    // On instancie la classe SettinPAram
    SettingParam {
        id: settingParam
    }

    Material.accent: styleApplication.accent;
    Material.theme: popupStyle.switch1ModeSombre.checked ? Material.Dark : Material.Light



    menuBar: MenuBar {
        font.family: "Tahoma"
        font.pointSize: 14

        Menu {
            title: qsTr("Trace GPS")
            font.family: "Tahoma"
            font.pointSize: 14

            MenuItem {
                text: qsTr("WindowParamétrage")
                onTriggered: popupStyle.show()
            }
            MenuItem {
                text: qsTr("Paramétrge carte")
                onTriggered: popupCarte.show()
            }
            MenuItem {
                Switch {
                    checked: styleApplication.theme
                }

            }

            MenuItem {
                text: qsTr("E&xit")
                onTriggered: Qt.quit()
            }
        }
    }



// Création de la Map principale
    MapComponent{
        id: map

    }


    // Création du PopupStyle pour le paramétrage du style
    PopupStyle{
       id:popupStyle
       onSignalAccentChanged: appWindow.changeAccent()
       onSignalThemeChanged: appWindow.changeTheme()
   }

    // Création du PopupStyle pour le paramétrage du style
    PopupCarte{
       id:popupCarte
       onEchelleChanged: {
           map.zoomLevel = popupCarte.sliderZoom.value
       }
//       onSignalSaveSettingMapChanged: appWindow.changeSaveSettingMAp()
   }

}
